package com.kolekcjeTrening;

import java.util.Scanner;

public class Main {

    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        NameDatabase names = new NameDatabase();

        String name;
        String noMoreNames = "-";

        do{
            System.out.println("Wpisz imie które zostanie dodane do zbióru:");
            System.out.println("Jeżeli chcesz skonczyć wpisz '-' ");
            name = getUserInput();
            if(!name.equals(noMoreNames)){
                names.NameDatabase(name);
            }
        }while(!name.equals(noMoreNames));

        System.out.println("Wprowadziles " + names.setWithNames.size() + " unikalnych imion.");
        System.out.println("Oto one:  " + names.setWithNames.toString());
    }

    private static String getUserInput() {
        return sc.nextLine().trim();
    }
}
